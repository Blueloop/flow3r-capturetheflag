# flow3r-CaptureTheFlag

## Setup

Copy the contents to your flow3r badge flash filesystem under the /sys/apps/ctf-game/ folder.
Enter your nickname in the file nick.json  
Restart your badge.

## Gameplay

Start the game with [badge]->[CaptureTheFlag]

You will join a random team (Red, Green, Blue) and start with 25 Lives. If you stay longer than 1 minute near min. 2 opponents it will decrease one live and after you reach 0 you will be assimilated by this team. Staying longer between teammates restore your health.

