from st3m.application import Application, ApplicationContext
from st3m.ui.colours import PUSH_RED, GO_GREEN, BLACK
from st3m.goose import Dict, Any
from st3m.input import InputState
from ctx import Context
import leds 

import json
import time
import math
import bluetooth
import random
import struct
import time
import binascii
import micropython
from micropython import const


# CTF Game-Einstellungen
maxTeams = 3
maxLives = 25

class Configuration:
    def __init__(self) -> None:
        self.key =  random.randint(100000, 999999)
        self.name = "CTF-Warrior%d" % self.key         
        self.team = random.randint(1,maxTeams)
        self.lives = maxLives;
        self.size: int = 75
        self.font: int = 5

    @classmethod
    def load(cls, path: str) -> "Configuration":
        res = cls()
        try:
            with open(path) as f:
                jsondata = f.read()
            data = json.loads(jsondata)
        except OSError:
            data = {}
        if "name" in data and type(data["name"]) == str:
            res.name = data["name"]
        if "size" in data:
            if type(data["size"]) == float:
                res.size = int(data["size"])
            if type(data["size"]) == int:
                res.size = data["size"]
        if "font" in data and type(data["font"]) == int:
            res.font = data["font"]
        if "key" in data and type(data["key"]) == int:
            res.key = data["key"]
        if "team" in data and type(data["team"]) == int:
            res.team = data["team"]
        if "lives" in data and type(data["lives"]) == int:
            res.lives = data["lives"]
        return res

    def save(self, path: str) -> None:
        d = {
            "name": self.name,
            "size": self.size,
            "font": self.font,
            "key":  self.key,
            "lives": self.lives,
            "team": self.team,
        }
        jsondata = json.dumps(d)
        with open(path, "w") as f:
            f.write(jsondata)
            f.close()


class CTFApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
                
        self._scale = 1.0
        self._blink = 0;
        self._led = 0.0
        self._phase = 0.0
        self._filename = "/flash/nick.json"
        self._config = Configuration.load(self._filename)

        self.ble = bluetooth.BLE()
        self.ble.irq(self._irq)
        self.ble.active(True)
        self.ble.gap_scan(0)

        # Daten für die BLE Beacons 
        self.data = bytearray(16)
        self.data[0] = 4    # CTF Kennung 4321
        self.data[1] = 3
        self.data[2] = 2
        self.data[3] = 1
        self.data[4] = self._config.team
        
        # Liste der Beacon-MACs 
        self.maclist = { }
        
        # Timer für 1x Sekunde
        self.lasttime = 0
        
        
        self.ble.gap_advertise(1000*50, adv_data=self.data, connectable=False)
        
    def _irq(self, event, data):
        _IRQ_SCAN_RESULT = const(5)

        if event == _IRQ_SCAN_RESULT:
            addr_type, addr, adv_type, rssi, adv_data = data
            addr_h = binascii.hexlify(bytes(addr)).decode('utf-8')
            adv_data_h = binascii.hexlify(bytes(adv_data)).decode('utf-8')
            #print('addr_type: {}, addr: {}, adv_type: {}, rssi: {}, adv_data: {}'.format(addr_type, addr_h, adv_type, rssi, adv_data_h))
            if ((adv_data[0] == 4) and (adv_data[1] == 3) and (adv_data[2] == 2) and (adv_data[3] == 1)):
                team = adv_data[4]
                print('Found Team: %s' % team)
                self.insertMac(addr_h,team)
                print(self.maclist)

    def insertMac(self, mac, team):
        if mac not in self.maclist:
            self.maclist[mac] = {"Team" : 0, "TTL" : 25}
        self.maclist[mac]["Team"] = team
        self.maclist[mac]["TTL"] =  60
            
    def removeMac(self):
        for k in self.maclist:
            self.maclist[k]["TTL"] -= 1
            print("Mac: %s TTL: %d" % (k,self.maclist[k]["TTL"]))
            if self.maclist[k]["TTL"] < 0:
                self.maclist.pop(k, None)
                    
    def countTeams(self):
        t = [0,0,0]
        for k in self.maclist:
            team = self.maclist[k]["Team"]
            if (team > 0) and (team < 4):
                t[team-1] += 1
        return t
    

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 28
        ctx.font = ctx.get_font_name(5)
        
        if self._config.team == 1:   ctx.rgb(0.8, 0, 0).rectangle(-120, -120, 240, 240).fill()
        elif self._config.team == 2: ctx.rgb(0, 0.8, 0).rectangle(-120, -120, 240, 240).fill()
        elif self._config.team == 3: ctx.rgb(0, 0, 0.8).rectangle(-120, -120, 240, 240).fill()
        else:                        ctx.rgb(0,  0,  0).rectangle(-120, -120, 240, 240).fill()
        

        ctx.rgb(1, 1, 1)
        y = -80
        ctx.move_to(0, y)
        ctx.text(self._config.name)
        y+=24
        ctx.move_to(0, y)
        if self._config.team == 1:   ctx.text("Team: RED")
        elif self._config.team == 2: ctx.text("Team: GREEN")
        elif self._config.team == 3: ctx.text("Team: BLUE")
        else: ctx.text("Team: ERROR!")
        y+=24
        ctx.move_to(0, y)
        ctx.text("Lives: %d" % self._config.lives)
        y+=24
        ctx.move_to(0, y)
        
        t = self.countTeams()
        
        ctx.text("Team RED: %d" % t[0])
        y+=24
        ctx.move_to(0, y)
        ctx.text("Team GREEN: %d" % t[1])
        y+=24
        ctx.move_to(0, y)
        ctx.text("Team BLUE: %d" % t[2])
        y+=24
        ctx.move_to(0, y)

        leds.set_all_rgb(32,32,32)
        if self._config.team == 1: leds.set_rgb(int(self._led), 255, 0, 0)
        if self._config.team == 2: leds.set_rgb(int(self._led), 0, 255, 0)
        if self._config.team == 3: leds.set_rgb(int(self._led), 0, 0, 255)

        leds.update()
        # ctx.fill()

    def on_exit(self) -> None:
        self._config.save(self._filename)
        
    def fight(self, teamA, teamB, teamC) -> None:
        if (teamA > 2) and (teamA > teamB) and (teamA > teamC):
                if self._config.team == teamA:
                    self._config.lives = maxLives
                else:
                    self._config.lives -= 1
                    if self._config.lives < 1:
                        self._config.lives = maxLives
                        self._config.team = teamA
         
    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        # Einmal die Sekunde
        if time.ticks_diff(time.ticks_ms(), self.lasttime) > 1000:
            self.lasttime = time.ticks_ms()

            # aeltere Eintraege löschen
            self.removeMac()
        
            t = self.countTeams()
            self.fight(t[0],t[1],t[2])
            self.fight(t[1],t[2],t[0])
            self.fight(t[2],t[0],t[1])
            self._config.save(self._filename)
        
        self._phase += delta_ms / 1000
        self._scale = abs(math.sin(self._phase))
        self._led += delta_ms / 4
        if self._led >= 40:
            self._led = 0

        self._blink += 1;
        